import java.lang.annotation.*;
import java.lang.reflect.Method;

public class Entity {

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    @interface Secured {
        int id();

        String login() default "strict";
    }

    @Secured(id = 0)
    private void privateMethod() {

    }

    @Secured(id = 1, login = "non-strict")
    public void publicMethod() {

    }

    @Secured(id = 2, login = "non-strict")
    void public2method() {

    }

    private Entity() {

    }

    public static void main(String[] args) {

        Class entity = Entity.class;

        try {
            entity.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        Method[] methods = entity.getDeclaredMethods();


        for (Method method : methods) {
            System.out.print(method);
            Annotation[] annotations = method.getDeclaredAnnotations();
            for(Annotation annotation : annotations){
                if(annotation instanceof Secured){
                    System.out.println(annotation);
                }
            }
        }

    }
}
